<?php

define('BASE_URL', dirname($_SERVER['SCRIPT_NAME']));

session_start();

require 'AltoRouter.php';
require 'bdd.php';
require './public/model/movie.php';
require './public/model/signin.php';
require './public/model/signup.php';

$router = new AltoRouter();

// === HOME === 
$router->map('GET', '/', function() {
    global $mysqli;

    if(!isset($_SESSION['user'])){
        header('Location: '.'/signin?=connect');
        die;
    }   

    $user_id = $_SESSION['user'];
    $user_favorites = getUserFavoriteID($mysqli, $user_id, 999);

    $mostViewed_movie = getMostViewed($mysqli, 5);
    $latest_movie = getLatest($mysqli, 10);

    require './public/template/home.php';
});

// === SEARCH MOVIES === 
$router->map('GET', '/movies/[a:search]', function($search){
    global $mysqli;

    $user_id = $_SESSION['user'];
    $user_favorites = getUserFavoriteID($mysqli, $user_id, 999);
    $movies = searchMovie($mysqli, str_replace('0ooooo0', '-', $search));

    require './public/template/search.php';

});

// === MOVIE INFOS === 
$router->map('GET', '/movie/[i:id]', function($id){
    global $mysqli;

    $user_id = $_SESSION['user'];
    $user_favorites = getUserFavoriteID($mysqli, $user_id, 999);
    $movie = getById($mysqli, $id);
    $actors = getActorsByID($mysqli, $id);
    $category = getCategory($mysqli, $id);
    $parental = getParental($mysqli, $id);

    require './public/template/movie.php';
});

// === ADD FAVORITE ===
$router->map('GET', '/favorite/[i:movie_id]', function($movie_id){
    global $mysqli;

    if(!isset($_SESSION['user'])){
        die();
    }

    $user_id = $_SESSION['user'];
    
    $like = likeMovie($mysqli, $user_id, $movie_id);    
});

// === USER DASHBOARD === 
$router->map('GET', '/dashboard', function(){
    global $mysqli;
    
    if(!isset($_SESSION['user'])){
        header('Location: '.'/signin?=connect');
        die;
    }   

    $user_id = $_SESSION['user'];
    $user_favorites = getUserFavoriteID($mysqli, $user_id, 999);
    $movie_categories = getCategories($mysqli);
    $latest_favorite = getUserFavorite($mysqli, $user_id, 5);
    $all_favorite = getUserFavorite($mysqli, $user_id, 999);

    require './public/template/dashboard.php';
});

// === SIGNIN === 
$router->map('GET', '/signin', function(){
    if(!isset($_SESSION['user'])){
        require './public/template/signin.php';   
        return; 
    }
    header('Location: '.'/');
    die;    
}, 'signin');

$router->map('POST', '/signin', function(){
    global $mysqli;
    $logged = false;

    if(isset($_POST['email']) && isset($_POST['password'])){        
        $email = $_POST['email'];
        $password = $_POST['password'];

       

        $logged = signinUser($mysqli, $email, $password);
    }

    if(!$logged){
        header('Location: '.'/signin?=error');
        die;
    }
    
    $_SESSION['user'] = $logged;

    header('Location: '.'/');
    die;
});

// === SIGNUP === 
$router->map('GET', '/signup', function(){
    if(isset($_SESSION['user'])){
        header('Location: '.'/');
        die;            
    }

    require './public/template/signup.php';    
}, 'signup');

$router->map('POST', '/signup', function(){
    global $mysqli;
    $logged = false;
    $error = false;

    if(!isset($_POST['email'])){
        $error = true;
    }
    if(!isset($_POST['firstname'])){
        $error = true;
    }
    if(!isset($_POST['lastname'])){
        $error = true;
    }
    if(!isset($_POST['password'])){
        $error = true;
    }

    if($error){
        header('Location: '.'/signup?=error');
        die;
    }

    $logged = signupUser($mysqli, $_POST['email'], $_POST['firstname'], $_POST['lastname'], $_POST['password']);

    header('Location: '.'/signin');
    die;  
});

// === LOGOUT ===
$router->map('GET', '/logout', function(){
    unset($_SESSION['user']);

    header('Location: '.'/');
    die;
}, 'logout');


// ===============
// ===============
// === ROUTING === 
$match = $router->match();
require './public/template/header.php';
if (is_array($match)) {
    
    if (is_callable($match['target'])) {
        call_user_func_array($match['target'], $match['params']);
    } else {
        $params = $match['params'];
        require './public/template/' . $match['target'] . '.php';
    }
   
} else {
    header('Location: '.'/');
    die;
}

require './public/template/footer.php';

?>
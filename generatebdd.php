<?php

$router->map('GET', '/categories', function(){
    global $mysqli;
    $string = file_get_contents('movieJson.json');

    $json_a = json_decode($string, true);

    $categories = $json_a['genres'];

    foreach($categories as $key => $category){
        var_dump($category);
        $mysqli->query("INSERT INTO categories (name) VALUES ('$category')");
    }
});

$router->map('GET', '/actors', function(){
    global $mysqli;
    $string = file_get_contents('movieJson.json');

    $json_a = json_decode($string, true);

    $actors = array();
    $movies = $json_a['movies'];

    foreach($movies as $key => $movie){
        $movie_actors = explode(', ', $movie['actors']);
        foreach($movie_actors as $akey => $actor){
            if(!in_array($actor, $actors)){
                $actors[] = $actor;
                $mysqli->query("INSERT INTO actors (name) VALUES ('$actor')");;
            }
        }
    }

    var_dump($actors);
});

$router->map('GET', '/movie_actors', function(){
    global $mysqli;
    
    $result = $mysqli->query("SELECT * FROM movie");
    $movies = $result -> fetch_all(MYSQLI_ASSOC);

    $result = $mysqli->query("SELECT * FROM actors");
    $actors = $result -> fetch_all(MYSQLI_ASSOC);

    foreach($movies as $key => $movie){
        $movie_actors = array_rand($actors, rand(2,15));
        $movie_id = $movie['id'];

        foreach($movie_actors as $ahey => $aid){
            $actor_id = $actors[$aid]['id'];
            $mysqli->query("INSERT INTO movie_actors (movie_id, actor_id) VALUES ('$movie_id','$actor_id')");             
        }        
    }
});

$router->map('GET', '/add_movies', function(){
    global $mysqli;
    $can_generate = false;
    $string = file_get_contents('movieJson.json');

    $json_a = json_decode($string, true);

    $movies = $json_a['movies'];

    $result = $mysqli->query("SELECT * FROM categories");
    $categories = $result -> fetch_all(MYSQLI_ASSOC);

    $result = $mysqli->query("SELECT * FROM parental");
    $parentals = $result -> fetch_all(MYSQLI_ASSOC);

    if(!$can_generate){
        return;
    }

    foreach($movies as $key => $movie){
        // var_dump($movie);
        $title = $movie['title'];
        $date = $movie['year'] . '-' . rand(1,12) . '-' . rand(1,28);
        $image = $movie['posterUrl'];
        $duration = $movie['runtime'];
        $description = $movie['plot'];
        $views = rand(0,10000);
        $category_id = $categories[array_rand($categories)]['id'];
        $parental_id = $parentals[array_rand($parentals)]['id'];
        
        $result = $mysqli->query("INSERT INTO movie (title, image, description, release_date, duration, category_id, parental_id, video, views) 
        VALUES ('$title', '$image', '$description', '$date', '$duration', '$category_id', '$parental_id', '', '$views') ");
        
    }
});

?>
const redirectMovie = () => {
    
    const movies = document.querySelectorAll('.movie_container article');
    movies.forEach((movie) => {
        movie.addEventListener('click', (event) => {
            if(!movie.hasAttribute('data-id')){
                return;
            }
            const movie_id = movie.getAttribute('data-id');
            if(!movie_id){
                return;
            }
            if(event.target.classList.contains('movie__love')){
                return;
            }

            window.location.href = `/movie/${movie_id}`;
        
        });
    });

}

const searchMovie = () => {
    const form = document.querySelector('.section_container.search form');
    if(!form){
        return;
    }
    form.addEventListener('submit', (event) => {
        event.preventDefault();
        const value = form.querySelector('input').value;
        window.location.href = `/movies/${value}`;
    });
}

const likeMovie = () => {
    const movies = document.querySelectorAll('.movie_container article');
    movies.forEach((movie) => {
        const love = movie.querySelector('.movie__love');
        love.addEventListener('click', (event)=>{
            const movie_id = movie.getAttribute('data-id');
            if(love.classList.contains('active')){
                love.classList.remove('active');
            }else{
                love.classList.add('active');
            }
            if(movie_id){
                const url = `/favorite/${movie_id}`;
                const xmlHttp = new XMLHttpRequest();
                xmlHttp.open( "GET", url, false );
                xmlHttp.send( null );
            }
            
        });
    });
}

likeMovie();
searchMovie();
redirectMovie();
<?php

function getMostViewed($mysqli, $number=5){

    $result = $mysqli->query("SELECT * FROM movie ORDER BY views DESC LIMIT $number");
    $movies = $result -> fetch_all(MYSQLI_ASSOC);
    return $movies;

}

function getLatest($mysqli, $number=10){
    $result = $mysqli->query("SELECT * FROM movie ORDER BY release_date DESC LIMIT $number");
    $movies = $result -> fetch_all(MYSQLI_ASSOC);
    return $movies;
}

function getById($mysqli, $id=0){
    $result = $mysqli->query("SELECT *, (SELECT AVG(user_evaluation.rate) FROM user_evaluation WHERE movie.id = user_evaluation.movie_id ) as evaluation FROM movie WHERE id = '$id'");
    $movie = $result->fetch_all(MYSQLI_ASSOC)[0];
    return $movie;
}

function getActorsByID($mysqli, $id=0){
    $result = $mysqli->query("SELECT name, a.id FROM movie as m, movie_actors as ma, actors as a WHERE ma.movie_id = $id AND m.id = $id AND ma.actor_id = a.id");
    $actors = $result->fetch_all(MYSQLI_ASSOC);
    return $actors;
}

function getCategory($mysqli, $id=0){
    $result = $mysqli->query("SELECT name, c.id FROM movie as m, categories as c WHERE m.category_id = c.id AND m.id = $id");
    $category = $result->fetch_all(MYSQLI_ASSOC);
    return $category;
}

function getParental($mysqli, $id=0){
    $result = $mysqli->query("SELECT name, c.id FROM movie as m, parental as c WHERE m.parental_id = c.id AND m.id = $id");
    $parental = $result->fetch_all(MYSQLI_ASSOC);
    return $parental;
}

function getCategories($mysqli){
    $result = $mysqli->query("SELECT * FROM categories");
    $categories = $result->fetch_all(MYSQLI_ASSOC);
    return $categories;
}

function getUserFavorite($mysqli, $user_id, $number=1){
    $result = $mysqli->query("SELECT movie.* FROM (movie, user_favorite) WHERE user_favorite.movie_id = movie.id AND user_favorite.user_id = $user_id ORDER BY user_favorite.date DESC LIMIT $number");
    $favorites = $result->fetch_all(MYSQLI_ASSOC);
    return $favorites;
}

function getUserFavoriteID($mysqli, $user_id, $number=1){
    $result = getUserFavorite($mysqli, $user_id, $number);
    $favorites = array();
    foreach($result as $key => $movie){
        $favorites[] = $movie['id'];
    }
    return $favorites;
}

function searchMovie($mysqli, $search){
    $result = $mysqli->query("SELECT DISTINCT m.* FROM movie as m, categories as c WHERE c.id = m.category_id AND c.name like '%$search%' OR m.title like '%$search%'");
    $movies = $result->fetch_all(MYSQLI_ASSOC);
    return $movies;
}

function likeMovie($mysqli, $user_id, $movie_id){
    $result = $mysqli->query("SELECT * FROM user_favorite WHERE user_id = '$user_id' AND movie_id = '$movie_id $'");
    $fav = $result->fetch_all(MYSQLI_ASSOC);
    $arr = (array)$fav;
    if(!empty($arr)){
        $mysqli->query("DELETE FROM user_favorite WHERE user_id = '$user_id' AND movie_id = '$movie_id'");
    }else{
        $today = date('Y-m-d');
        $mysqli->query("INSERT INTO user_favorite (date, user_id, movie_id) VALUES ('$today', '$user_id', '$movie_id')");
    }
    return $fav;
}

?>
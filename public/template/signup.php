<div class="sign_page">
    <div class="form_container">
        <form action="/signup" method="post">
            <h2 class="form__title">Signup</h2>
            <div class="form__row">
                <div class="form__input_container">
                    <label for="email">E-mail</label>
                    <input type="email" name="email" id="signup_email" placeholder="E-mail">
                </div>            
            </div>

            <div class="form__row">
                <div class="form__input_container">
                    <label for="firstname">Firstname</label>
                    <input type="text" name="firstname" id="signup_firstname" placeholder="Firstname">
                </div>
            </div>

            <div class="form__row">
                <div class="form__input_container">
                    <label for="lastname">Lastname</label>
                    <input type="text" name="lastname" id="signup_lastname" placeholder="Lastname">
                </div>
            </div>

            <div class="form__row">
                <div class="form__input_container">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="signup_password" placeholder="Password">
                </div>
            </div>

            <div class="form__row">
                <button type="submit" name="submit" id="signup_submit">Signup</button>
            </div>

        </form>
    </div>

    <div class="form__redirect">
        <span>You have an account ? <a href="/signin"> signin </a></span>
    </div>
</div>

<div class="section_container search">
    <h2>Search movie</h2>
    <form action="/search">
        <input type="text" name="s" placeholder="Title, Categorie, ...">
    </form>
</div>

<div class="section_container categories">
    <h2>Movie categories</h2>
    <div class="movie_specifications__dot">
        <?php
            foreach($movie_categories as $key => $cat){
            ?>
                <a href="/movies/<?= strtolower(str_replace('-', '0ooooo0', $cat['name'])) ?>"><?= $cat['name'] ?></a>
            <?php
            }
        ?>
    </div>
</div>


<div class="section_container latest_favorites">

    <h2>My latest favorites</h2>

    <div class="movie_container">
        <?php
        foreach ($latest_favorite as $key => $movie) {
        ?>
            <article data-id="<?= $movie['id'] ?>">
                <div class="movie__image"><img src="<?= $movie['image'] ?>"></div>
                <div class="movie__title"><?= $movie['title'] ?></div>

                <div class="movie__date">
                    <?php
                    $date = explode('-', $movie['release_date']);
                    echo $date[1] . '/' . $date[0];
                    ?>
                </div>

                <div class="movie__views"><?= $movie['views'] ?> views</div>

                <?php
                    $active_like = '';
                    if(in_array($movie['id'], $user_favorites)){
                        $active_like = 'active';
                    }
                ?>
                <div class="movie__love <?= $active_like ?>">                    
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M19.5 12.572l-7.5 7.428l-7.5 -7.428m0 0a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572"></path>
                    </svg>
                </div>
            </article>
        <?php
        }
        ?>
    </div>

</div>

<div class="section_container all_favorites">

    <h2>My all favorites</h2>

    <div class="movie_container">
        <?php
        foreach ($all_favorite as $key => $movie) {
        ?>
            <article data-id="<?= $movie['id'] ?>">
                <div class="movie__image"><img src="<?= $movie['image'] ?>"></div>
                <div class="movie__title"><?= $movie['title'] ?></div>

                <div class="movie__date">
                    <?php
                    $date = explode('-', $movie['release_date']);
                    echo $date[1] . '/' . $date[0];
                    ?>
                </div>

                <div class="movie__views"><?= $movie['views'] ?> views</div>

                <?php
                    $active_like = '';
                    if(in_array($movie['id'], $user_favorites)){
                        $active_like = 'active';
                    }
                ?>
                <div class="movie__love <?= $active_like ?>">                    
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M19.5 12.572l-7.5 7.428l-7.5 -7.428m0 0a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572"></path>
                    </svg>
                </div>
            </article>
        <?php
        }
        ?>
    </div>

</div>

<div class="section_container statistics">
    <h2>Statistics</h2>
    <div>
        <span>5 movies watched</span><br>
        <span><?= count($all_favorite) ?> movies in favorite</span><br>
        <span><?= date('H:m Y, F j') ?></span>
    </div>
</div>

<div class="section_container profile">
    <h2>Profile</h2>
    ...
</div>
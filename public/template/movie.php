<div class="section_container movie_informations">
    <div class="movie__banner">
        <img src="<?= $movie['image'] ?>">
    </div>
    <div class="movie__specifications">
        <h1><?= $movie['title'] ?></h1>
        <p><?= $movie['description'] ?></p>
        <span class="movie_specifications__evaluation">User's opinion : <?= intval($movie['evaluation']) ?>/5</span>
        <span class="movie_specifications__duration"><?= $movie['duration'] ?> minutes</span>
        <span class="movie_specifications__date">Release: <?= $movie['release_date'] ?></span>

        <div class="movie_specifications__categories movie_specifications__dot"> <b> Categories </b><br>
        <?php
            foreach($category as $key => $cat){
            ?>
                <a href="/movies/<?= strtolower(str_replace('-', '0ooooo0', $cat['name'])) ?>"><?= $cat['name'] ?></a>
            <?php
            }
        ?>
        </div>
        <div class="movie_specifications__actors  movie_specifications__dot"> <b> Actors </b><br>
        <?php
            foreach($actors as $key => $actor){
            ?>
                <a href="/actor/<?= $actor['id'] ?>"><?= $actor['name'] ?></a>
            <?php
            }
        ?>
        </div>
        <div class="movie_specifications__parental  movie_specifications__dot"> <b> Parental advisor </b><br>
        <?php
            foreach($parental as $key => $par){
            ?>
                <a href="/parental/<?= $par['id'] ?>"><?= $par['name'] ?></a>
            <?php
            }
        ?>
        </div>
        
    </div>

    <div class="movie__trailer">
        <?php 
            $ytb_short = $movie['video'];
            if(empty($ytb_short)){
                $ytb_short = 'nM4iy0reaCA';
            }else{
                $ytb_short = end(explode('/', $ytb_short));
            }            
        ?>
        <iframe src="https://www.youtube.com/embed/<?= $ytb_short ?>?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
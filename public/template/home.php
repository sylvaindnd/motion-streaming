<div class="section_container mostviewed_movie">

    <h2>Most viewed movies</h2>

    <div class="movie_container">
        <?php
        foreach ($mostViewed_movie as $key => $movie) {
        ?>
            <article data-id="<?= $movie['id'] ?>">
                <div class="movie__image"><img src="<?= $movie['image'] ?>"></div>
                <div class="movie__title"><?= $movie['title'] ?></div>

                <div class="movie__date">
                    <?php
                    $date = explode('-', $movie['release_date']);
                    echo $date[1] . '/' . $date[0];
                    ?>
                </div>

                <div class="movie__views"><?= $movie['views'] ?> views</div>

                <?php
                    $active_like = '';
                    if(in_array($movie['id'], $user_favorites)){
                        $active_like = 'active';
                    }
                ?>
                <div class="movie__love <?= $active_like ?>">                    
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M19.5 12.572l-7.5 7.428l-7.5 -7.428m0 0a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572"></path>
                    </svg>
                </div>

            </article>
        <?php
        }
        ?>
    </div>

</div>

<div class="section_container latest_movie">
    <h2>Lastest movies</h2>

    <div class="movie_container">
        <?php
        foreach ($latest_movie as $key => $movie) {
        ?>
            <article data-id="<?= $movie['id'] ?>">
                <div class="movie__image"><img src="<?= $movie['image'] ?>"></div>
                <div class="movie__title"><?= $movie['title'] ?></div>
                <div class="movie__date">
                    <?php
                    $date = explode('-', $movie['release_date']);
                    echo $date[1] . '/' . $date[0];
                    ?>
                </div>
                <div class="movie__views"><?= $movie['views'] ?> views</div>

                <?php
                    $active_like = '';
                    if(in_array($movie['id'], $user_favorites)){
                        $active_like = 'active';
                    }
                ?>
                <div class="movie__love <?= $active_like ?>">                    
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M19.5 12.572l-7.5 7.428l-7.5 -7.428m0 0a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572"></path>
                    </svg>
                </div>
            </article>
        <?php
        }
        ?>
    </div>

</div>

<div class="section_container pricing">
    <h2>Pricing</h2>

    <div class="pricing_container">
        <div>
            <span><b>10€</b>/mois</span>
        </div>

        <div class="big_pricing">
            <span><b>100€</b>/an</span>
        </div>
    </div>
</div>

<div class="section_container presentation">
    <h2>Presentation</h2>

    <p>Voici pourquoi notre service est fait pour vous ! <br> Découvrez des milliers de films volés fait rien que pour vous.</p>

    <div class="presentation_container">
        <iframe src="https://www.youtube.com/embed/G8rAw_WKf8E?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <iframe src="https://www.youtube.com/embed/luS_wweosxg?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <iframe src="https://www.youtube.com/embed/2XJCWxj6Tr4?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
<div class="sign_page">
    <div class="form_container">
        <form action="/signin" method="post">
            <h2 class="form__title">Signin</h2>
            <div class="form__row">
                <div class="form__input_container">
                    <label for="email">E-mail</label>
                    <input type="email" name="email" id="signin_email" placeholder="E-mail">
                </div>            
            </div>

            <div class="form__row">
                <div class="form__input_container">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="signin_password" placeholder="Password">
                </div>
            </div>

            <div class="form__row">
                <button type="submit" name="submit" id="signin_submit">Signin</button>
            </div>

        </form>
    </div>

    <div class="form__redirect">
        <span>You don't have an account yet ? <a href="/signup"> signup </a></span>
    </div>
</div>

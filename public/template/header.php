
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= BASE_URL; ?>public/assets/css/style.css">
    <title>Motion Streaming</title>
</head>
<body>

    <?php 
        if(isset($_SESSION['user'])){
            ?>
                <div class="nav">
                    <div class="nav__title">
                        <a href="/">Motion streaming</a>
                    </div>
                    <div class="nav__menu">
                        <div class="nav_menu__col">
                            <a href="/dashboard">Dashboard</a>
                        </div>

                        <div class="nav_menu__col">
                            <a href="/logout">Logout</a>
                        </div>
                    </div>
                </div>
            <?php
        }
    ?>
   

    <div class="container">
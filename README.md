# Motion Streaming



## Basic Overview

Motion Streaming is a streaming movie application. This application allows you to watch hundreds of pirated movies from all over the world. This application is linked with a MySQL database. All the data concerning the films or the users are present in this database.

## Installation

- Transfer files

    > git clone

    Transfer the files to a new folder with the command `git clone https://gitlab.com/sylvaindnd/motion-streaming.git`

- PHP Server

    You need a PHP engine like [MAMP](https://www.mamp.info/en/downloads/)

- MySQL

    You can get a database with [MAMP](https://www.mamp.info/en/downloads/)

## How to use
- SignUp

    > `/signup`

    The registration page allows you to register new users.

- SignIn

    > `/signin`

    Access the user account and the pages protected by registration.

- LogOut

    > `/logout`

    Log out of a user account.

- DashBoard

    > `/dashboard`

    Access all user information.

- Search movies

    > `/movies/[str:search]`

    Allows you to find a film in a search engine. With a query it is possible to find films by name or by category.

- Information movie

    > `/movie/[int:movieID]`

    Allows you to display all the information about a movie from its ID..